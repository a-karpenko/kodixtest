"use strict";

/* параметры для gulp-autoprefixer */
var autoprefixerList = [
    'Chrome >= 45',
    'Firefox ESR',
    'Edge >= 12',
    'Explorer >= 10',
    'iOS >= 9',
    'Safari >= 9',
    'Android >= 4.4',
    'Opera >= 30'
];

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts/'
    },
    src: {
        html: 'src/*.html',
        jsInternal: 'src/js/internal.js',
        jsExternal: 'src/js/external.js',
        styleInternal: 'src/style/internal.scss',
        styleExternal: 'src/style/external.scss',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        css: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        fonts: 'srs/fonts/**/*.*'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: './build'
    },
    notify: false
};

var gulp = require('gulp'),
    webserver = require('browser-sync'),
    plumber = require('gulp-plumber'),
    fileinclude = require('gulp-file-include'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    jpegrecompress = require('imagemin-jpeg-recompress'),
    pngquant = require('imagemin-pngquant'),
    del = require('del'),
    rename = require('gulp-rename');

/* задачи */
gulp.task('webserver', function() {
    webserver(config);
});

gulp.task('html:build', function() {
    return gulp.src(path.src.html)
        .pipe(plumber())
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file',
            indent: true
        }))
        .pipe(gulp.dest(path.build.html))
        .pipe(webserver.reload({
            stream: true
        }));
});

gulp.task('css:build', [
    'css-internal',
    'css-external'
]);

gulp.task('css-internal', function() {
    return gulp.src(path.src.styleInternal)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS())
        .pipe(rename({
            extname: '.css'
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(webserver.reload({
            stream: true
        }));
});

gulp.task('css-external', function() {
    return gulp.src(path.src.styleExternal)
        .pipe(plumber())
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: autoprefixerList
        }))
        .pipe(cleanCSS())
        .pipe(rename({
            extname: '.css'
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(webserver.reload({
            stream: true
        }));
});

gulp.task('js:build', [
    'js-internal',
    'js-external'
]);

gulp.task('js-internal', function() {
    return gulp.src(path.src.jsInternal)
        .pipe(plumber())
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file',
            indent: true
        }))

        .pipe(gulp.dest(path.build.js))
        .pipe(webserver.reload({
            stream: true
        }));
});

gulp.task('js-external', function() {
    return gulp.src(path.src.jsExternal)
        .pipe(plumber())
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file',
            indent: true
        }))
        //.pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(webserver.reload({
            stream: true
        }));
});

gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('image:build', function() {
    return gulp.src(path.src.img)
        .pipe(imagemin([
            imagemin.gifsicle({
                interlaced: true
            }),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false
                }]
            })
        ]))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('clean:build', function() {
    del.sync(path.clean);
});

gulp.task('build', [
    'clean:build',
    'html:build',
    'css:build',
    'js:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function() {
    gulp.watch(path.watch.html, ['html:build']);
    gulp.watch(path.watch.css, ['css:build']);
    gulp.watch(path.watch.js, ['js:build']);
    gulp.watch(path.watch.img, ['image:build']);
    gulp.watch(path.watch.fonts, ['fonts:build']);
});

gulp.task('default', [
    'clean:build',
    'build',
    'webserver',
    'watch'
]);
