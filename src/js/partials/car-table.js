var CarTable = function() {
    var self = this,
        $tableName = $('.js-car-table'),
        cars = [],
        carMaxId = 0,
        carUrl = "https://rawgit.com/Varinetz/e6cbadec972e76a340c41a65fcc2a6b3/raw/90191826a3bac2ff0761040ed1d95c59f14eaf26/frontend_test_table.json",
        isRebuilded = false;

    self.init = $.getJSON(carUrl, function(data) {
        $.each(data, function(i, item) {
            var carItem = {};

            for (var key in item) {
                if (key == 'id') {
                    carMaxId = (carMaxId < item[key]) ? item[key] : carMaxId;
                }

                if (key == 'title') {
                    var searchTitle = item[key].match(/\d+\.\d+ \S+? .+? \d+ л.с. (?:МКПП|АКПП|CVT|РКПП)/i);
                    carItem[key] = searchTitle ? searchTitle[0] : '';
                    continue;
                }
                carItem[key] = item[key];
            }

            cars.push(carItem);
            self.insertItem(carItem);
        });

        self.tableRebuild(false);
    }).fail(function(jqxhr, textStatus, error) {
        var err = textStatus + ", " + error;
        console.log("Ошибка: " + err);
    });

    self.getStatus = function(status) {
        switch (status) {
            case 'pednding':
                return 'Ожидается';
            case 'out_of_stock':
                return 'Нет в наличии';
            case 'in_stock':
                return 'В наличии';

            default:
                return '';
        }
    }

    self.insertItem = function(carItem) {
        var title = carItem.title || '',
            year = carItem.year || '',
            id = carItem.id || '',
            description = carItem.description ? '<p class="table__subdesc">' + carItem.description + '</p>' : '',
            color = carItem.color ? '<div class="color"><span class="color__label color__label--' + carItem.color + '"></span></div>' : '',
            status = self.getStatus(carItem.status),
            price = carItem.price ? carItem.price.toLocaleString() + " руб." : '';

        $tableName.addClass('is-visible');
        $tableName.find('tbody').append("<tr data-index=" + id + "><td class='table__item table__item--title'>" + title + description + "</td><td class='table__item table__item--year'>" + year + "</td><td class='table__item table__item--color'>" + color + "</td><td class='table__item table__item--status'>" + status + "</td><td class='table__item table__item--price'>" + price + "</td><td class='table__item table__item--delete'><button class='table__delete js-car-delete'>Удалить</button></td></tr>");
    }

    self.addItem = function() {
        var carItem = {},
            formData = $(this).serializeArray();

        event.preventDefault();
        carMaxId++;
        carItem.id = carMaxId;

        $.each(formData, function(i, item) {
            if ((item.name == 'year') || (item.name == 'price')) item.value = +item.value
            carItem[item.name] = item.value;
        });

        cars.push(carItem);
        self.insertItem(carItem);
        self.tableRebuild(true);
    }

    self.deleteItem = function() {
        var $tr = $(this).closest('tr'),
            removeId = $tr.data('index');

        $tr.remove();
        cars.splice(removeId, 1);
        if ($tableName.find('tbody tr').length < 1) $tableName.removeClass('is-visible');
    }


    self.tableRebuild = function(force) {
        if (($(window).width() < breakpoint)) {
            if ((!isRebuilded) || force) {
                $.each($tableName.find('.table__item .table__subdesc'), function() {
                    $(this).parent().after($(this));
                });

                isRebuilded = true;
            }
        } else {
              $.each($tableName.find('.table__subdesc'), function() {
                $(this).siblings('.table__item--title').append($(this));

            });

            isRebuilded = false;
        }
    }
}

if ($('.js-car-table').length > 0) {
    var carTable = new CarTable();
    var resizeTable;

    $(document).on('click', '.js-car-delete', carTable.deleteItem);
    $(document).on('submit', '.js-form-submit', carTable.addItem);

    $(window).resize(function(e) {
        clearTimeout(resizeTable);
        resizeTable = setTimeout(function() {
            carTable.tableRebuild(false);
        }, 100);
    });
}
