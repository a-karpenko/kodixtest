function initLabelAnimation($element) {
    var field = $element.find('input, textarea');
    if ($(field).val().trim() != '') {
        $element.addClass('is-filled');
    }

    $(field).on('focus', function(e) {
        $element.addClass('is-filled');
        $element.addClass('is-animated')
    }).on('blur', function(e) {
        if ($(this).val().trim() === '') {
            $element.removeClass('is-filled');
            $element.removeClass('is-animated');
        }
    });
}

if ($('.js-label-animation').length > 0) {
    $('.js-label-animation').each(function(index, el) {
        initLabelAnimation($(el));
    });
}

if ($('.js-select').length > 0) {
    $('.js-select').each(function() {
        var ph = $(this).data('placeholder');

        $(this).select2({
            minimumResultsForSearch: -1,
            placeholder: ph
        });
    });
}
