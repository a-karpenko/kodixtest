Parsley.setLocale('ru');

$.extend(Parsley.options, {
    trigger: 'blur change', // change нужен для select'а
    validationThreshold: '0',
    errorsWrapper: '<div></div>',
    errorTemplate: '<p class="parsley-error-message"></p>',
    classHandler: function(instance) {
        var $element = instance.$element,
            type = $element.attr('type'),
            $handler;
        if (type == 'checkbox' || type == 'radio') {
            $handler = $element; // то есть ничего не выделяем (input скрыт), иначе выделяет родительский блок
        } else if ($element.hasClass('select2-hidden-accessible')) {
            $handler = $('.select2-selection--single', $element.next('.select2'));
        }

        return $handler;
    },
    errorsContainer: function(instance) {
        var $element = instance.$element,
            type = $element.attr('type'),
            $container;

        if (type == 'checkbox' || type == 'radio') {
            $container = $('[name="' + $element.attr('name') + '"]:last + label').next('.errors-placement');
        } else if ($element.hasClass('select2-hidden-accessible')) {
            $container = $element.next('.select2').next('.errors-placement');
        } 

        return $container;
    }
});

// Кастомные валидаторы
// Проверка названия авто
Parsley.addValidator('carName', {
    validateString: function(value) {
        return /\d+\.\d+ \S+? .+? \d+ л.с. (?:МКПП|АКПП|CVT|РКПП)/i.test(value);
    },
    messages: {
        ru: 'Объём Трансмиссия Модель Мощность л.с КПП'
    }
});

// Проверка года выпуска
Parsley.addValidator('minDate', {
    validateNumber: function(value, requirement) {
        var today = new Date().getFullYear();

        if ((value >= requirement) && (value <= today)) {
            return true;
        } else {
            return false;
        }
    },
    requirementType: 'integer',
    messages: {
        ru: 'Неверный год выпуска'
    }
});

// Только цифры
Parsley.addValidator('number', {
    validateString: function(value) {
        return /^[0-9]*$/i.test(value);
    },
    messages: {
        ru: 'Cимволы 0-9'
    }
});


// Ошибки для нетипичных элементов
Parsley.on('field:init', function() {
    var $element = this.$element,
        type = $element.attr('type'),
        $block = $('<div/>').addClass('errors-placement'),
        $last;

    if (type == 'checkbox' || type == 'radio') {
        $last = $('[name="' + $element.attr('name') + '"]:last + label');
        if (!$last.next('.errors-placement').length) {
            $last.after($block);
        }
    } else if ($element.hasClass('select2-hidden-accessible')) {
        $last = $element.next('.select2');
        if (!$last.next('.errors-placement').length) {
            $last.after($block);
        }
    }
});

$('form[data-validate="true"]').parsley();
